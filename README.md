### TODO
1. copy database
1. a lighter deploy script
1. test it all

### Playbooks
1. ansible-playbook prod.yml (updates the prod machines)
1. ansible-playbook master.yml (updates the master machines)
1. ansible-playbook worker.yml (updates the worker machines)
1. ansible-playbook copy_down_db.xml (copies database from prod to localhost)
1. ansible-playbook push_up_db.xml (copies database from localhost to prod)
1. ansible-playbook deploy.xml (git pull, django stuff, restart daemons)

### Inventory
1. create/destroy host on digital ocean
1. add/remove it to/from the hosts file (in root dir of this repo)
1. commit it to the repo